# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'rsa.ui'
#
# Created by: PyQt5 UI code generator 5.9.2
#
# WARNING! All changes made in this file will be lost!
# from Package import encText, decText
from PyQt5 import QtCore, QtGui, QtWidgets
from layout import encText, decText, register, encDoc, decDoc, encArch, decArch, about, helpr, upgrade, trial
from layout.Package import licence
import os
from uuid import getnode as get_mac
##################################################################
######### Variable Global essentiel pour la gestion de licence ###
##################################################################
mac = ':'.join(("%012X" % get_mac())[i:i + 2] for i in range(0, 12, 2))

# """
# * class de construction de l'interface d'accueil
# """


class Ui_MainWindow(object):

    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(735, 573)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(MainWindow.sizePolicy().hasHeightForWidth())
        MainWindow.setSizePolicy(sizePolicy)
        MainWindow.setContextMenuPolicy(QtCore.Qt.DefaultContextMenu)
        MainWindow.setAcceptDrops(False)
        MainWindow.setWindowOpacity(2.0)
        MainWindow.setLayoutDirection(QtCore.Qt.LeftToRight)
        MainWindow.setAutoFillBackground(False)
        icon0 = QtGui.QIcon()
        icon0.addPixmap(QtGui.QPixmap("images/icons8_bluelock.ico"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        MainWindow.setWindowIcon(icon0)
        MainWindow.setStyleSheet("background-color:#272E3F;")
        MainWindow.setUnifiedTitleAndToolBarOnMac(False)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setStyleSheet("background:#272E3F;")
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName("gridLayout")
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem, 2, 0, 1, 1)
        spacerItem1 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Maximum)
        self.gridLayout.addItem(spacerItem1, 1, 1, 1, 1)
        spacerItem2 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem2, 2, 2, 1, 1)
        spacerItem3 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Maximum)
        self.gridLayout.addItem(spacerItem3, 3, 1, 1, 1)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.groupBox_2 = QtWidgets.QGroupBox(self.centralwidget)
        self.groupBox_2.setAutoFillBackground(False)
        self.groupBox_2.setStyleSheet("background-color:white;")
        self.groupBox_2.setObjectName("groupBox_2")
        self.horizontalLayout_8 = QtWidgets.QHBoxLayout(self.groupBox_2)
        self.horizontalLayout_8.setObjectName("horizontalLayout_8")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout()
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.frame_2 = QtWidgets.QFrame(self.groupBox_2)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.frame_2.sizePolicy().hasHeightForWidth())
        self.frame_2.setSizePolicy(sizePolicy)
        self.frame_2.setLayoutDirection(QtCore.Qt.RightToLeft)
        self.frame_2.setStyleSheet("background-color:white;\n"
"border-style:outset;\n"
"border-width:2px;\n"
"border-radius:10px;\n"
"font: bold 2px;\n"
"min-width: 10em;\n"
"padding:1px;\n"
"border-color:#00AA00;")
        self.frame_2.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_2.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_2.setObjectName("frame_2")
        self.horizontalLayout_11 = QtWidgets.QHBoxLayout(self.frame_2)
        self.horizontalLayout_11.setContentsMargins(-1, -1, -1, 9)
        self.horizontalLayout_11.setObjectName("horizontalLayout_11")
        self.label_9 = QtWidgets.QLabel(self.frame_2)
        self.label_9.setAlignment(QtCore.Qt.AlignCenter)
        self.label_9.setObjectName("label_9")
        self.horizontalLayout_11.addWidget(self.label_9)
        self.verticalLayout_2.addWidget(self.frame_2)
        self.label = QtWidgets.QLabel(self.groupBox_2)
        self.label.setSizeIncrement(QtCore.QSize(0, 0))
        self.label.setBaseSize(QtCore.QSize(0, 0))
        self.label.setStyleSheet("color:black;")
        self.label.setTextFormat(QtCore.Qt.AutoText)
        self.label.setScaledContents(False)
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.label.setWordWrap(False)
        self.label.setObjectName("label")
        self.verticalLayout_2.addWidget(self.label)
        self.label_5 = QtWidgets.QLabel(self.groupBox_2)
        self.label_5.setStyleSheet("color:black;")
        self.label_5.setAlignment(QtCore.Qt.AlignCenter)
        self.label_5.setObjectName("label_5")
        self.verticalLayout_2.addWidget(self.label_5)
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.encryptText = QtWidgets.QPushButton(self.groupBox_2)
        self.encryptText.setStyleSheet("background-color:#AECFFF;\n"
"border-style:outset;\n"
"border-width:2px;\n"
"border-radius:10px;\n"
"font: bold 12px;\n"
"min-width: 10em;\n"
"border-color:transparent;")
        self.encryptText.setObjectName("encryptText")
        self.horizontalLayout_3.addWidget(self.encryptText)
        spacerItem4 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_3.addItem(spacerItem4)
        self.decryptText = QtWidgets.QPushButton(self.groupBox_2)
        self.decryptText.setStyleSheet("background-color:#AECFFF;\n"
"border-style:outset;\n"
"border-width:2px;\n"
"border-radius:10px;\n"
"font: bold 12px;\n"
"min-width: 10em;\n"
"border-color:transparent;")
        self.decryptText.setObjectName("decryptText")
        self.horizontalLayout_3.addWidget(self.decryptText)
        self.verticalLayout_2.addLayout(self.horizontalLayout_3)
        self.horizontalLayout_8.addLayout(self.verticalLayout_2)
        self.label.raise_()
        self.label_5.raise_()
        self.horizontalLayout.addWidget(self.groupBox_2)
        spacerItem5 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem5)
        self.groupBox_4 = QtWidgets.QGroupBox(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.groupBox_4.sizePolicy().hasHeightForWidth())
        self.groupBox_4.setSizePolicy(sizePolicy)
        self.groupBox_4.setContextMenuPolicy(QtCore.Qt.DefaultContextMenu)
        self.groupBox_4.setStyleSheet("background-color:white;")
        self.groupBox_4.setFlat(False)
        self.groupBox_4.setObjectName("groupBox_4")
        self.horizontalLayout_7 = QtWidgets.QHBoxLayout(self.groupBox_4)
        self.horizontalLayout_7.setObjectName("horizontalLayout_7")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.frame_3 = QtWidgets.QFrame(self.groupBox_4)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.frame_3.sizePolicy().hasHeightForWidth())
        self.frame_3.setSizePolicy(sizePolicy)
        self.frame_3.setBaseSize(QtCore.QSize(0, 0))
        self.frame_3.setLayoutDirection(QtCore.Qt.RightToLeft)
        self.frame_3.setStyleSheet("background-color:white;\n"
"border-style:outset;\n"
"border-width:2px;\n"
"border-radius:10px;\n"
"font: bold 2px;\n"
"min-width: 10em;\n"
"padding:1px;\n"
"border-color:#FF0000;")
        self.frame_3.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_3.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_3.setObjectName("frame_3")
        self.horizontalLayout_12 = QtWidgets.QHBoxLayout(self.frame_3)
        self.horizontalLayout_12.setObjectName("horizontalLayout_12")
        self.label_11 = QtWidgets.QLabel(self.frame_3)
        self.label_11.setAlignment(QtCore.Qt.AlignCenter)
        self.label_11.setObjectName("label_11")
        self.horizontalLayout_12.addWidget(self.label_11)
        self.verticalLayout.addWidget(self.frame_3)
        self.label_2 = QtWidgets.QLabel(self.groupBox_4)
        self.label_2.setStyleSheet("color:black;")
        self.label_2.setScaledContents(False)
        self.label_2.setAlignment(QtCore.Qt.AlignCenter)
        self.label_2.setOpenExternalLinks(False)
        self.label_2.setObjectName("label_2")
        self.verticalLayout.addWidget(self.label_2)
        self.label_10 = QtWidgets.QLabel(self.groupBox_4)
        self.label_10.setStyleSheet("color:black;")
        self.label_10.setAlignment(QtCore.Qt.AlignCenter)
        self.label_10.setObjectName("label_10")
        self.verticalLayout.addWidget(self.label_10)
        self.label_8 = QtWidgets.QLabel(self.groupBox_4)
        self.label_8.setAlignment(QtCore.Qt.AlignCenter)
        self.label_8.setObjectName("label_8")
        self.verticalLayout.addWidget(self.label_8)
        self.horizontalLayout_4 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        self.encryptArch = QtWidgets.QPushButton(self.groupBox_4)
        self.encryptArch.setStyleSheet("background-color:#AECFFF;\n"
"border-style:outset;\n"
"border-width:2px;\n"
"border-radius:10px;\n"
"font: bold 12px;\n"
"min-width: 10em;\n"
"border-color:transparent;")
        self.encryptArch.setObjectName("encryptArch")
        self.horizontalLayout_4.addWidget(self.encryptArch)
        spacerItem6 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_4.addItem(spacerItem6)
        self.decryptArch = QtWidgets.QPushButton(self.groupBox_4)
        self.decryptArch.setStyleSheet("background-color:#AECFFF;\n"
"border-style:outset;\n"
"border-width:2px;\n"
"border-radius:10px;\n"
"font: bold 12px;\n"
"min-width: 10em;\n"
"border-color:transparent;")
        self.decryptArch.setObjectName("decryptArch")
        self.horizontalLayout_4.addWidget(self.decryptArch)
        self.verticalLayout.addLayout(self.horizontalLayout_4)
        self.horizontalLayout_7.addLayout(self.verticalLayout)
        self.label_2.raise_()
        self.label_8.raise_()
        self.label_10.raise_()
        self.horizontalLayout.addWidget(self.groupBox_4)
        self.gridLayout.addLayout(self.horizontalLayout, 2, 1, 1, 1)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.groupBox_3 = QtWidgets.QGroupBox(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.groupBox_3.sizePolicy().hasHeightForWidth())
        self.groupBox_3.setSizePolicy(sizePolicy)
        self.groupBox_3.setStyleSheet("background-color:white;")
        self.groupBox_3.setObjectName("groupBox_3")
        self.horizontalLayout_9 = QtWidgets.QHBoxLayout(self.groupBox_3)
        self.horizontalLayout_9.setObjectName("horizontalLayout_9")
        self.verticalLayout_3 = QtWidgets.QVBoxLayout()
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.frame_4 = QtWidgets.QFrame(self.groupBox_3)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.frame_4.sizePolicy().hasHeightForWidth())
        self.frame_4.setSizePolicy(sizePolicy)
        self.frame_4.setLayoutDirection(QtCore.Qt.RightToLeft)
        self.frame_4.setStyleSheet("background-color:white;\n"
"border-style:outset;\n"
"border-width:2px;\n"
"border-radius:10px;\n"
"font: bold 2px;\n"
"min-width: 10em;\n"
"padding:1px;\n"
"border-color:#FF0000;")
        self.frame_4.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_4.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_4.setObjectName("frame_4")
        self.horizontalLayout_13 = QtWidgets.QHBoxLayout(self.frame_4)
        self.horizontalLayout_13.setObjectName("horizontalLayout_13")
        self.label_12 = QtWidgets.QLabel(self.frame_4)
        self.label_12.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.label_12.setObjectName("label_12")
        self.horizontalLayout_13.addWidget(self.label_12)
        self.verticalLayout_3.addWidget(self.frame_4)
        self.label_3 = QtWidgets.QLabel(self.groupBox_3)
        self.label_3.setStyleSheet("color:black;")
        self.label_3.setAlignment(QtCore.Qt.AlignCenter)
        self.label_3.setObjectName("label_3")
        self.verticalLayout_3.addWidget(self.label_3)
        self.label_6 = QtWidgets.QLabel(self.groupBox_3)
        self.label_6.setStyleSheet("color:black;")
        self.label_6.setAlignment(QtCore.Qt.AlignCenter)
        self.label_6.setObjectName("label_6")
        self.verticalLayout_3.addWidget(self.label_6)
        self.label_7 = QtWidgets.QLabel(self.groupBox_3)
        self.label_7.setAlignment(QtCore.Qt.AlignCenter)
        self.label_7.setObjectName("label_7")
        self.verticalLayout_3.addWidget(self.label_7)
        self.horizontalLayout_5 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_5.setObjectName("horizontalLayout_5")
        self.encryptDoc = QtWidgets.QPushButton(self.groupBox_3)
        self.encryptDoc.setStyleSheet("background-color:#AECFFF;\n"
"border-style:outset;\n"
"border-width:2px;\n"
"border-radius:10px;\n"
"font: bold 12px;\n"
"min-width: 10em;\n"
"border-color:transparent;")
        self.encryptDoc.setObjectName("encryptDoc")
        self.horizontalLayout_5.addWidget(self.encryptDoc)
        spacerItem7 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_5.addItem(spacerItem7)
        self.decryptDoc = QtWidgets.QPushButton(self.groupBox_3)
        self.decryptDoc.setStyleSheet("background-color:#AECFFF;\n"
"border-style:outset;\n"
"border-width:2px;\n"
"border-radius:10px;\n"
"font: bold 12px;\n"
"min-width: 10em;\n"
"border-color:transparent;")
        self.decryptDoc.setObjectName("decryptDoc")
        self.horizontalLayout_5.addWidget(self.decryptDoc)
        self.verticalLayout_3.addLayout(self.horizontalLayout_5)
        self.horizontalLayout_9.addLayout(self.verticalLayout_3)
        self.horizontalLayout_2.addWidget(self.groupBox_3)
        spacerItem8 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem8)
        self.frame_5 = QtWidgets.QFrame(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.frame_5.sizePolicy().hasHeightForWidth())
        self.frame_5.setSizePolicy(sizePolicy)
        self.frame_5.setStyleSheet("background-color:white;")
        self.frame_5.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_5.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_5.setObjectName("frame_5")
        self.horizontalLayout_10 = QtWidgets.QHBoxLayout(self.frame_5)
        self.horizontalLayout_10.setObjectName("horizontalLayout_10")
        self.verticalLayout_4 = QtWidgets.QVBoxLayout()
        self.verticalLayout_4.setObjectName("verticalLayout_4")
        self.frame_6 = QtWidgets.QFrame(self.frame_5)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.frame_6.sizePolicy().hasHeightForWidth())
        self.frame_6.setSizePolicy(sizePolicy)
        self.frame_6.setLayoutDirection(QtCore.Qt.RightToLeft)
        self.frame_6.setStyleSheet("background-color:white;\n"
"border-style:outset;\n"
"border-width:2px;\n"
"border-radius:10px;\n"
"font: bold 2px;\n"
"min-width: 10em;\n"
"padding:1px;\n"
"border-color:#FF0000;")
        self.frame_6.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_6.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_6.setObjectName("frame_6")
        self.horizontalLayout_14 = QtWidgets.QHBoxLayout(self.frame_6)
        self.horizontalLayout_14.setObjectName("horizontalLayout_14")
        self.label_13 = QtWidgets.QLabel(self.frame_6)
        self.label_13.setObjectName("label_13")
        self.horizontalLayout_14.addWidget(self.label_13)
        self.verticalLayout_4.addWidget(self.frame_6)
        self.label_4 = QtWidgets.QLabel(self.frame_5)
        self.label_4.setStyleSheet("color:black;")
        self.label_4.setAlignment(QtCore.Qt.AlignCenter)
        self.label_4.setObjectName("label_4")
        self.verticalLayout_4.addWidget(self.label_4)
        self.horizontalLayout_6 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_6.setObjectName("horizontalLayout_6")
        self.encryptRepert = QtWidgets.QPushButton(self.frame_5)
        self.encryptRepert.setStyleSheet("background-color:#AECFFF;\n"
"border-style:outset;\n"
"border-width:2px;\n"
"border-radius:10px;\n"
"font: bold 12px;\n"
"min-width: 10em;\n"
"border-color:transparent;")
        self.encryptRepert.setObjectName("encryptRepert")
        self.horizontalLayout_6.addWidget(self.encryptRepert)
        spacerItem9 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_6.addItem(spacerItem9)
        self.decryptRepert = QtWidgets.QPushButton(self.frame_5)
        self.decryptRepert.setStyleSheet("background-color:#AECFFF;\n"
"border-style:outset;\n"
"border-width:2px;\n"
"border-radius:10px;\n"
"font: bold 12px;\n"
"min-width: 10em;\n"
"border-color:transparent;")
        self.decryptRepert.setObjectName("decryptRepert")
        self.horizontalLayout_6.addWidget(self.decryptRepert)
        self.verticalLayout_4.addLayout(self.horizontalLayout_6)
        self.horizontalLayout_10.addLayout(self.verticalLayout_4)
        self.horizontalLayout_2.addWidget(self.frame_5)
        self.gridLayout.addLayout(self.horizontalLayout_2, 4, 1, 1, 1)
        self.logo = QtWidgets.QLabel(self.centralwidget)
        self.logo.setText("")
        self.logo.setPixmap(QtGui.QPixmap("images/logo3.png"))
        self.logo.setObjectName("logo")
        self.gridLayout.addWidget(self.logo, 0, 1, 1, 1, QtCore.Qt.AlignHCenter)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setEnabled(True)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 735, 21))
        self.menubar.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.menubar.setAutoFillBackground(False)
        self.menubar.setStyleSheet("background:darkgray;")
        self.menubar.setNativeMenuBar(False)
        self.menubar.setObjectName("menubar")
        self.menuFile = QtWidgets.QMenu(self.menubar)
        self.menuFile.setStyleSheet("")
        self.menuFile.setObjectName("menuFile")
        self.menurecent = QtWidgets.QMenu(self.menuFile)
        self.menurecent.setObjectName("menurecent")
        self.menuView = QtWidgets.QMenu(self.menubar)
        self.menuView.setObjectName("menuView")
        self.menuOption = QtWidgets.QMenu(self.menubar)
        self.menuOption.setObjectName("menuOption")
        self.menuHelp = QtWidgets.QMenu(self.menubar)
        self.menuHelp.setObjectName("menuHelp")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.actionopen = QtWidgets.QAction(MainWindow)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("images/icons8_file_2.ico"), QtGui.QIcon.Normal, QtGui.QIcon.On)
        self.actionopen.setIcon(icon)
        self.actionopen.setObjectName("actionopen")
        self.actionsave = QtWidgets.QAction(MainWindow)
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap("images/icons8_save.ico"), QtGui.QIcon.Normal, QtGui.QIcon.On)
        self.actionsave.setIcon(icon1)
        self.actionsave.setObjectName("actionsave")
        self.actionlogout = QtWidgets.QAction(MainWindow)
        self.actionlogout.triggered.connect(self.closeW)
        icon2 = QtGui.QIcon()
        icon2.addPixmap(QtGui.QPixmap("images/icons8_exit.ico"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        icon2.addPixmap(QtGui.QPixmap("images/icons8_exit.ico"), QtGui.QIcon.Normal, QtGui.QIcon.On)
        self.actionlogout.setIcon(icon2)
        self.actionlogout.setObjectName("actionlogout")
        self.actionrecent_files = QtWidgets.QAction(MainWindow)
        self.actionrecent_files.setObjectName("actionrecent_files")
        self.actionfull_screen = QtWidgets.QAction(MainWindow)
        self.actionfull_screen.triggered.connect(self.fullW)
        icon3 = QtGui.QIcon()
        icon3.addPixmap(QtGui.QPixmap("images/icons8_full_screen.ico"), QtGui.QIcon.Normal, QtGui.QIcon.On)
        self.actionfull_screen.setIcon(icon3)
        self.actionfull_screen.setObjectName("actionfull_screen")
        self.actionshort_screen = QtWidgets.QAction(MainWindow)
        self.actionshort_screen.triggered.connect(self.minW)
        icon4 = QtGui.QIcon()
        icon4.addPixmap(QtGui.QPixmap("images/icons8_compress.ico"), QtGui.QIcon.Normal, QtGui.QIcon.On)
        self.actionshort_screen.setIcon(icon4)
        self.actionshort_screen.setObjectName("actionshort_screen")
        self.actionedit_background_color = QtWidgets.QAction(MainWindow)
        self.actionedit_background_color.triggered.connect(self.colorW)
        icon5 = QtGui.QIcon()
        icon5.addPixmap(QtGui.QPixmap("images/icons8_color_palette.ico"), QtGui.QIcon.Normal, QtGui.QIcon.On)
        self.actionedit_background_color.setIcon(icon5)
        self.actionedit_background_color.setObjectName("actionedit_background_color")
        self.actionreset = QtWidgets.QAction(MainWindow)
        self.actionreset.triggered.connect(self.restW)
        icon6 = QtGui.QIcon()
        icon6.addPixmap(QtGui.QPixmap("images/icons8_time_machine.ico"), QtGui.QIcon.Normal, QtGui.QIcon.On)
        self.actionreset.setIcon(icon6)
        self.actionreset.setObjectName("actionreset")
        self.actionrefresh = QtWidgets.QAction(MainWindow)
        icon7 = QtGui.QIcon()
        icon7.addPixmap(QtGui.QPixmap("images/icons8_refresh.ico"), QtGui.QIcon.Normal, QtGui.QIcon.On)
        self.actionrefresh.setIcon(icon7)
        self.actionrefresh.setObjectName("actionrefresh")
        self.actionmark = QtWidgets.QAction(MainWindow)
        icon8 = QtGui.QIcon()
        icon8.addPixmap(QtGui.QPixmap("images/icons8_bookmark_ribbon.ico"), QtGui.QIcon.Normal, QtGui.QIcon.On)
        self.actionmark.setIcon(icon8)
        self.actionmark.setObjectName("actionmark")
        self.actionhelp = QtWidgets.QAction(MainWindow)
        self.actionhelp.triggered.connect(self.helpW)
        icon9 = QtGui.QIcon()
        icon9.addPixmap(QtGui.QPixmap("images/icons8_help.ico"), QtGui.QIcon.Normal, QtGui.QIcon.On)
        self.actionhelp.setIcon(icon9)
        self.actionhelp.setObjectName("actionhelp")
        self.actionabout = QtWidgets.QAction(MainWindow)
        self.actionabout.triggered.connect(self.aboutW)
        icon10 = QtGui.QIcon()
        icon10.addPixmap(QtGui.QPixmap("images/icons8_info.ico"), QtGui.QIcon.Normal, QtGui.QIcon.On)
        self.actionabout.setIcon(icon10)
        self.actionabout.setObjectName("actionabout")
        self.actionpurchare = QtWidgets.QAction(MainWindow)
        self.actionpurchare.triggered.connect(self.activeW)
        icon11 = QtGui.QIcon()
        icon11.addPixmap(QtGui.QPixmap("images/icons8_alcoholic_beverage_licensing.ico"), QtGui.QIcon.Normal, QtGui.QIcon.On)
        self.actionpurchare.setIcon(icon11)
        self.actionpurchare.setObjectName("actionpurchare")
        # self.actionhome_page = QtWidgets.QAction(MainWindow)
        # icon12 = QtGui.QIcon()
        # icon12.addPixmap(QtGui.QPixmap("images/icons8_home.ico"), QtGui.QIcon.Normal, QtGui.QIcon.On)
        # self.actionhome_page.setIcon(icon12)
        # self.actionhome_page.setObjectName("actionhome_page")
        self.actioncheck_for_update = QtWidgets.QAction(MainWindow)
        self.actioncheck_for_update.triggered.connect(self.upGradW)
        icon13 = QtGui.QIcon()
        icon13.addPixmap(QtGui.QPixmap("images/icons8_upgrade_1.ico"), QtGui.QIcon.Normal, QtGui.QIcon.On)
        self.actioncheck_for_update.setIcon(icon13)
        self.actioncheck_for_update.setObjectName("actioncheck_for_update")
        self.actionTrial_learn_of_RSA = QtWidgets.QAction(MainWindow)
        self.actionTrial_learn_of_RSA.triggered.connect(self.TrialRsa)
        icon14 = QtGui.QIcon()
        icon14.addPixmap(QtGui.QPixmap("images/icons8_experiment_trial_filled.ico"), QtGui.QIcon.Normal, QtGui.QIcon.On)
        self.actionTrial_learn_of_RSA.setIcon(icon14)
        self.actionTrial_learn_of_RSA.setObjectName("actionTrial_learn_of_RSA")
        self.menurecent.addAction(self.actionrecent_files)
        self.menuFile.addAction(self.actionopen)
        self.menuFile.addAction(self.menurecent.menuAction())
        self.menuFile.addAction(self.actionsave)
        self.menuFile.addAction(self.actionlogout)
        self.menuView.addAction(self.actionfull_screen)
        self.menuView.addAction(self.actionshort_screen)
        self.menuView.addAction(self.actionedit_background_color)
        self.menuView.addAction(self.actionreset)
        self.menuOption.addAction(self.actionrefresh)
        self.menuOption.addAction(self.actionmark)
        self.menuOption.addSeparator()
        self.menuOption.addAction(self.actionTrial_learn_of_RSA)
        self.menuHelp.addAction(self.actionhelp)
        self.menuHelp.addAction(self.actionabout)
        self.menuHelp.addAction(self.actionpurchare)
        # self.menuHelp.addAction(self.actionhome_page)
        self.menuHelp.addAction(self.actioncheck_for_update)
        self.menubar.addAction(self.menuFile.menuAction())
        self.menubar.addAction(self.menuView.menuAction())
        self.menubar.addAction(self.menuOption.menuAction())
        self.menubar.addAction(self.menuHelp.menuAction())

        self.retranslateUi(MainWindow)
        self.encryptText.clicked.connect(self.encryptionText)
        self.decryptText.clicked.connect(self.decryptionText)
        self.encryptArch.clicked.connect(self.encyptionArch)
        self.decryptArch.clicked.connect(self.decryptionArch)
        self.encryptDoc.clicked.connect(self.encryptionDoc)
        self.decryptDoc.clicked.connect(self.decryptionDoc)
        self.encryptRepert.clicked.connect(self.encryptionRepert)
        self.decryptRepert.clicked.connect(self.decryptionRepert)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)
        MainWindow.setTabOrder(self.encryptText, self.decryptText)
        MainWindow.setTabOrder(self.decryptText, self.encryptDoc)
        MainWindow.setTabOrder(self.encryptDoc, self.decryptDoc)
        MainWindow.setTabOrder(self.decryptDoc, self.encryptArch)
        MainWindow.setTabOrder(self.encryptArch, self.decryptArch)
        MainWindow.setTabOrder(self.decryptArch, self.encryptRepert)
        MainWindow.setTabOrder(self.encryptRepert, self.decryptRepert)
        MainWindow.setShortcutEnabled(True)

    def encryptionText(self):
        Dialog = QtWidgets.QDialog()
        ui = encText.encText()
        ui.setupUi(Dialog)
        Dialog.exec_()

    def closeW(self):
        exit()

    def aboutW(self):
        Dialog = QtWidgets.QDialog()
        ui = about.About()
        ui.setupUi(Dialog)
        Dialog.exec_()

    def colorW(self):
        color = QtWidgets.QColorDialog.getColor()
        # print(color.name())
        self.centralwidget.setStyleSheet("background-color:" + str(color.name()) + ";")

    def restW(self):
        self.centralwidget.setStyleSheet("background:#272E3F;")

    def minW(self):
        MainWindow.showNormal()

    def fullW(self):
        MainWindow.showMaximized()

    def TrialRsa(self):
        Dialog = QtWidgets.QDialog()
        # print(0)
        ui = trial.trial()
        # print(1)
        ui.setupUi(Dialog)
        # print(2)
        Dialog.exec_()
    
    def upGradW(self):
        Dialog = QtWidgets.QDialog()
        ui = upgrade.Ui_Upgrade()
        ui.setupUi(Dialog)
        Dialog.exec_()

    def helpW(self):
        Dialog = QtWidgets.QDialog()
        ui = helpr.Ui_Help()
        ui.setupUi(Dialog)
        Dialog.exec_()

    def activeW(self):
        Dialog = QtWidgets.QDialog()
        ui = register.Ui_Register()
        ui.setupUi(Dialog)
        Dialog.exec_()

    def decryptionText(self):
        Dialog = QtWidgets.QDialog()
        ui = decText.decText()
        ui.setupUi(Dialog)
        Dialog.exec_()

    def encyptionArch(self):
        lic = licence.licence()
        licen = ""
        if lic.existL("licence") == True:
            with open("licence", "r", encoding="utf-8") as fil:
                for line in fil:
                    licen = lic.decrypt(line)
            if licen != "True":
                Dialog = QtWidgets.QDialog()
                ui = register.Ui_Register()
                ui.setupUi(Dialog)
                Dialog.exec_()
            else:
                Dialog = QtWidgets.QDialog()
                ui = encArch.encArch()
                ui.setupUi(Dialog)
                Dialog.exec_()
        else:
            lic.write("licence")

    def decryptionArch(self):
        lic = licence.licence()
        licen = ""
        if lic.existL("licence") == True:
            with open("licence", "r", encoding="utf-8") as fil:
                for line in fil:
                    licen = lic.decrypt(line)
            if licen != "True":
                Dialog = QtWidgets.QDialog()
                ui = register.Ui_Register()
                ui.setupUi(Dialog)
                Dialog.exec_()
            else:
                Dialog = QtWidgets.QDialog()
                ui = decArch.decArch()
                ui.setupUi(Dialog)
                Dialog.exec_()
        else:
            lic.write("licence")

    def encryptionDoc(self):
        lic = licence.licence()
        licen = ""
        if lic.existL("licence") == True:
            with open("licence", "r", encoding="utf-8") as fil:
                for line in fil:
                    licen = lic.decrypt(line)
            if licen != "True":
                Dialog = QtWidgets.QDialog()
                ui = register.Ui_Register()
                ui.setupUi(Dialog)
                Dialog.exec_()
            else:
                Dialog = QtWidgets.QDialog()
                ui = encDoc.encDoc()
                ui.setupUi(Dialog)
                Dialog.exec_()
        else:
            lic.write("licence")

    def decryptionDoc(self):
        lic = licence.licence()
        licen = ""
        if lic.existL("licence") == True:
            with open("licence", "r", encoding="utf-8") as fil:
                for line in fil:
                    licen = lic.decrypt(line)
            if licen != "True":
                Dialog = QtWidgets.QDialog()
                ui = register.Ui_Register()
                ui.setupUi(Dialog)
                Dialog.exec_()
            else:
                Dialog = QtWidgets.QDialog()
                ui = decDoc.decDoc()
                ui.setupUi(Dialog)
                Dialog.exec_()
        else:
            lic.write("licence")

    def encryptionRepert(self):
        lic = licence.licence()
        licen = ""
        if lic.existL("licence") == True:
            with open("licence", "r", encoding="utf-8") as fil:
                for line in fil:
                    licen = lic.decrypt(line)
            if licen != "True":
                Dialog = QtWidgets.QDialog()
                ui = register.Ui_Register()
                ui.setupUi(Dialog)
                Dialog.exec_()
            else:
                print()
        else:
            lic.write("licence")

    def decryptionRepert(self):
        lic = licence.licence()
        licen = ""
        if lic.existL("licence") == True:
            with open("licence", "r", encoding="utf-8") as fil:
                for line in fil:
                    licen = lic.decrypt(line)
            if licen != "True":
                Dialog = QtWidgets.QDialog()
                ui = register.Ui_Register()
                ui.setupUi(Dialog)
                Dialog.exec_()
            else:
                print()
        else:
            lic.write("licence")

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "ICT-RSA SECURITY"))
        self.label_9.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-size:12pt; font-weight:600; color:#00aa00;\">Hot</span></p></body></html>"))
        self.label.setText(_translate("MainWindow", "TEXT"))
        self.label_5.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-weight:600;\">Encrypt and decrypt any length of text message</span></p></body></html>"))
        self.encryptText.setText(_translate("MainWindow", "ENCRYPT"))
        self.decryptText.setText(_translate("MainWindow", "DECRYPT"))
        self.label_11.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-size:12pt; font-weight:600; color:#ff0000;\">Pro</span></p></body></html>"))
        self.label_2.setText(_translate("MainWindow", "ARCHIVES"))
        self.label_10.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-weight:600;\">Encrypt and decrypt all format document like:</span></p></body></html>"))
        self.label_8.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-size:9pt; font-style:italic; color:#000000;\">.iso, .daa, .zip, .rar, .7z, ....</span></p></body></html>"))
        self.encryptArch.setText(_translate("MainWindow", "ENCRYPT"))
        self.decryptArch.setText(_translate("MainWindow", "DECRYPT"))
        self.label_12.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-size:12pt; font-weight:600; color:#ff0000;\">Pro</span></p></body></html>"))
        self.label_3.setText(_translate("MainWindow", "DOCUMENT"))
        self.label_6.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-weight:600;\">Encrypt and decrypt all format document like:</span></p></body></html>"))
        self.label_7.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-size:9pt; font-style:italic; color:#000000;\">.docx, .pptx, .jpeg, .png, .asta, ....</span></p></body></html>"))
        self.encryptDoc.setText(_translate("MainWindow", "ENCRYPT"))
        self.decryptDoc.setText(_translate("MainWindow", "DECRYPT"))
        self.label_13.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-size:12pt; font-weight:600; color:#ff0000;\">Pro</span></p></body></html>"))
        self.label_4.setText(_translate("MainWindow", "REPERTOIRE"))
        self.encryptRepert.setText(_translate("MainWindow", "ENCRYPT"))
        self.decryptRepert.setText(_translate("MainWindow", "DECRYPT"))
        self.menuFile.setTitle(_translate("MainWindow", "File"))
        self.menurecent.setTitle(_translate("MainWindow", "recent file"))
        self.menuView.setTitle(_translate("MainWindow", "View"))
        self.menuOption.setTitle(_translate("MainWindow", "Option"))
        self.menuHelp.setTitle(_translate("MainWindow", "Help"))
        self.actionopen.setText(_translate("MainWindow", "open"))
        self.actionopen.setShortcut(_translate("MainWindow", "Ctrl+O"))
        self.actionsave.setText(_translate("MainWindow", "save"))
        self.actionsave.setShortcut(_translate("MainWindow", "Ctrl+S"))
        self.actionlogout.setText(_translate("MainWindow", "quit"))
        self.actionlogout.setShortcut(_translate("MainWindow", "Ctrl+Q"))
        self.actionrecent_files.setText(_translate("MainWindow", "recent files"))
        self.actionfull_screen.setText(_translate("MainWindow", "full screen"))
        self.actionfull_screen.setShortcut(_translate("MainWindow", "Ctrl+Up"))
        self.actionshort_screen.setText(_translate("MainWindow", "short screen"))
        self.actionshort_screen.setShortcut(_translate("MainWindow", "Ctrl+Down"))
        self.actionedit_background_color.setText(_translate("MainWindow", "edit background color"))
        self.actionedit_background_color.setShortcut(_translate("MainWindow", "Ctrl+L"))
        self.actionreset.setText(_translate("MainWindow", "reset"))
        self.actionrefresh.setText(_translate("MainWindow", "refresh"))
        self.actionrefresh.setShortcut(_translate("MainWindow", "Ctrl+R"))
        self.actionmark.setText(_translate("MainWindow", "mark"))
        self.actionmark.setShortcut(_translate("MainWindow", "Ctrl+U"))
        self.actionhelp.setText(_translate("MainWindow", "help"))
        self.actionhelp.setShortcut(_translate("MainWindow", "F1"))
        self.actionabout.setText(_translate("MainWindow", "about ICT-RSA SECURITY"))
        self.actionabout.setShortcut(_translate("MainWindow", "F2"))
        self.actionpurchare.setText(_translate("MainWindow", "activation"))
        self.actionpurchare.setShortcut(_translate("MainWindow", "F3"))
        # self.actionhome_page.setText(_translate("MainWindow", "home page"))
        # self.actionhome_page.setShortcut(_translate("MainWindow", "Ctrl+T"))
        self.actioncheck_for_update.setText(_translate("MainWindow", "check for update"))
        self.actioncheck_for_update.setShortcut(_translate("MainWindow", "F5"))
        self.actionTrial_learn_of_RSA.setShortcut(_translate("MainWindow", "F9"))
        self.actionTrial_learn_of_RSA.setText(_translate("MainWindow", "Trial learn of RSA"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())
