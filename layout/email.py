# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'email.ui'
#
# Created by: PyQt5 UI code generator 5.9.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Email(object):
    def setupUi(self, Email):
        Email.setObjectName("Email")
        Email.resize(400, 300)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("images/icons8_bluelock.ico"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        Email.setWindowIcon(icon)
        Email.setStyleSheet("background-color:#272E3F;")
        self.gridLayout = QtWidgets.QGridLayout(Email)
        self.gridLayout.setObjectName("gridLayout")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.label_2 = QtWidgets.QLabel(Email)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_2.sizePolicy().hasHeightForWidth())
        self.label_2.setSizePolicy(sizePolicy)
        self.label_2.setStyleSheet("color:white;")
        self.label_2.setAlignment(QtCore.Qt.AlignCenter)
        self.label_2.setWordWrap(True)
        self.label_2.setObjectName("label_2")
        self.verticalLayout.addWidget(self.label_2)
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Maximum)
        self.verticalLayout.addItem(spacerItem)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.label = QtWidgets.QLabel(Email)
        self.label.setStyleSheet("color:white;")
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.label.setObjectName("label")
        self.horizontalLayout.addWidget(self.label)
        self.email = QtWidgets.QLineEdit(Email)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.email.sizePolicy().hasHeightForWidth())
        self.email.setSizePolicy(sizePolicy)
        self.email.setStyleSheet("color:white;")
        self.email.setObjectName("email")
        self.horizontalLayout.addWidget(self.email)
        self.verticalLayout.addLayout(self.horizontalLayout)
        spacerItem1 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Maximum)
        self.verticalLayout.addItem(spacerItem1)
        self.btn_active = QtWidgets.QPushButton(Email)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.btn_active.sizePolicy().hasHeightForWidth())
        self.btn_active.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setPointSize(14)
        self.btn_active.setFont(font)
        self.btn_active.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.btn_active.setStyleSheet("color:white;\n"
"background-color:darkgray;")
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap("images/icons8_email_1.ico"), QtGui.QIcon.Normal, QtGui.QIcon.On)
        self.btn_active.setIcon(icon1)
        self.btn_active.setObjectName("btn_active")
        self.verticalLayout.addWidget(self.btn_active)
        self.gridLayout.addLayout(self.verticalLayout, 0, 0, 1, 1)

        self.retranslateUi(Email)
        QtCore.QMetaObject.connectSlotsByName(Email)

    def retranslateUi(self, Email):
        _translate = QtCore.QCoreApplication.translate
        Email.setWindowTitle(_translate("Email", "Email"))
        self.label_2.setText(_translate("Email", "<html><head/><body><p><span style=\" font-size:10pt; font-weight:600; font-style:italic;\">Please Hint your email to receive notification of softwware</span></p></body></html>"))
        self.label.setText(_translate("Email", "<html><head/><body><p><span style=\" font-size:10pt; font-weight:600;\">Email ::</span></p></body></html>"))
        self.email.setPlaceholderText(_translate("Email", "example@domain.cm"))
        self.btn_active.setText(_translate("Email", "ok"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Email = QtWidgets.QDialog()
    ui = Ui_Email()
    ui.setupUi(Email)
    Email.show()
    sys.exit(app.exec_())
