#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 17 16:16:45 2020

@author: root
"""

import re
def is_allowed(string):
    return bool(re.search(r"[.rar][.iso]", string))
print (is_allowed("tes.rar"))