import random
import os


class licence:

    def __init__(self):
        self.lic = "True"

    def getLic(self):
        return self.lic

    '''
    Euclid's algorithm for determining the greatest common divisor
    Use iteration to make it faster for larger integers
    '''

    def gcd(self, a, b):
        while b != 0:
            a, b = b, a % b
        return a

    '''
    Euclid's extended algorithm for finding the multiplicative inverse of two numbers
    '''

    def generateur_d(self, e, phi):
        i = 1
        while (((i * phi + 1) % e) != 0):
            # print("{}:{}".format(i,((i*pp + 1) %self.e)))
            i += 1
        return int(((i * phi + 1) / e))

    '''
    Tests to see if a number is prime.
    '''

    def is_prime(self, num):
        if num == 2:
            return True
        if num < 2 or num % 2 == 0:
            return False
        for n in range(3, int(num ** 0.5) + 2, 2):
            if num % n == 0:
                return False
        return True

    '''
    generate a pair key of communication
    '''

    def generate_keypair(self):
        p, q = 17, 23
        # print("{}:{}".format(p, q))
        n = p * q

        # Phi is the totient of n
        phi = (p - 1) * (q - 1)

        # Choose an integer e such that e and phi(n) are coprime
        e = random.randrange(1, phi)

        # Use Euclid's Algorithm to verify that e and phi(n) are comprime
        g = self.gcd(e, phi)
        while g != 1:
            e = random.randrange(1, phi)
            g = self.gcd(e, phi)

        # Use Extended Euclid's Algorithm to generate the private key
        d = self.generateur_d(e, phi)

        # Return public and private keypair
        # Public key is (e, n) and private key is (d, n)
        # public, private = ((e, n), (d, n))
        return ((e, n), (d, n))

    '''
    encrypt text message
    '''

    def encrypt(self, plaintext):
        # Unpack the key into it's components
        key, n = (267, 391)
        # Convert each letter in the plaintext to numbers based on the character using a^b mod m
        cipher = [chr((ord(char) ** key) % n) for char in plaintext]
        # Return the array of bytes
        return ''.join(map(lambda x: str(x), cipher))

    '''
    decrypt text message
    '''

    def decrypt(self, ciphertext):
        # Unpack the key into its components
        key, n = (323, 391)
        # Generate the plaintext based on the ciphertext and key using a^b mod m
        plain = [chr((ord(char) ** key) % n) for char in ciphertext]
        # Return the array of bytes as a string
        return ''.join(plain)

    def read(self, path):
        with open(path, 'r') as fil:
            if not fil:
                print("file not found")
            else:
                self.lic = self.decrypt(fil.readline())
                return self.decrypt(fil.readline())

    def write(self, path):
        if os.path.exists(path) :
            os.remove(path)
        with open(path, 'a+', encoding="utf-8") as fil:
            if not fil:
                print("file not found")
            else:
                fil.write(self.encrypt(self.lic))

    def setLic(self, lic):
        self.lic = lic

    def existL(self, path):
        return os.path.exists(path)

# if __name__ == "__main__":
#     licence = licence()
#     print((licence.encrypt("true")).encode("utf-8"))
