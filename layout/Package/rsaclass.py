import random, math
import os
from stat import S_IREAD, S_IRGRP, S_IROTH, S_IWUSR, S_IWRITE
import re
import threading as th
from PyQt5.QtWidgets import *
  
 
class RSA:

    def __init__(self,p=0,q=0):
        self.private = ()
        self.public = ()
        self.cypher = ""
        self.plaintext = ""
        self.e = 0
        self.phi = 0
        self.d = 0
        self.p = p
        self.q = q
        self.n = 0

    def getPub(self):
        return self.public

    def getPriv(self):
        return self.private
    
    def getE(self):
        return self.e
    
    def getPhi(self):
        return self.phi
    
    def getN(self):
        return self.n
    
    def getD(self):
        return self.d
    
    def setP(self,p):
        self.p = p

    def setE(self,e):
        self.e = e
    
    def setQ(self,q):
        self.q=q
    
    def setKeys(self, pk):
        # print("{}".format(str(pk)))
        self.public = pk

    def setPrk(self, pk):
        # print("{}".format(str(pk)))
        self.private = pk

    '''
    Euclid's algorithm for determining the greatest common divisor
    Use iteration to make it faster for larger integers
    '''

    def gcd(self, a, b):
        while b != 0:
            a, b = b, a % b
        return a

    '''
    Euclid's extended algorithm for finding the multiplicative inverse of two numbers
    '''

    def generateur_d(self, e, phi):
        i = 1
        while (((i * phi + 1) % e) != 0):
            # print("{}:{}".format(i,((i*self.pp + 1) %self.e)))
            i += 1
        return int(((i * phi + 1) / e))


    def generateur_de(self, e, phi):
        i = 1
        while (((i * phi + 1) % e) != 0):
            # print("{}:{}".format(i,((i*self.pp + 1) %self.e)))
            i += 1
        return int(((i * phi + 1) / e))
    '''
    Tests to see if a number is prime.
    '''

    def is_prime(self, num):
        if num == 2:
            return True
        if num < 2 or num % 2 == 0:
            return False

        for n in range(2, int(math.sqrt(num))+1):
            if num % n == 0:
                return False
        return True

    '''(135, 2047
    generate a pair key of communication
    '''
    '''
    here p and q is define by user
    '''
    #check is the input p is prime
    def pIsPrime(self):
        return self.is_prime(self.p)
    
    #check is the input q is prime
    def qIsPrime(self):
        return self.is_prime(self.q)
    
    def GK(self):
        if self.qIsPrime() & self.pIsPrime():
            #compute n
            self.n = self.p * self.q
            print(self.q)
            print(self.p)
            #compute phi
            self.phi = (self.p - 1) * (self.q - 1)
            #random e between max(p,q) and phi and (e,p)=0 or prime
            PrimesList = self.ListPrimeLessThanPhi((lambda x,y:(x>y and x or y))(self.p,self.q),self.phi)
            if self.e ==0 | self.is_prime(self.e)==False:
                liste = self.computeE(PrimesList)
                self.e = liste[random.randrange(0,len(liste))]
            print(self.e)
            # Use Extended Euclid's Algorithm to generate the private key
            self.d = self.generateur_de(self.e, self.phi)
            print(self.d)
            # Return public and private keypair
            # Public key is (e, n) and private key is (d, n)
            self.public, self.private = ((self.e, self.n), (self.d, self.n))
            # print(self.private)
            # print(self.public)
            return True
        return False
    '''
    here p = q = 0
    '''
    def computeE(self,l):
        ListE = list()
        for i in range(len(l)):
            if self.gcd(l[i],self.phi)==1:
                ListE.append(l[i])
        return ListE

    def ListPrimeLessThanPhi(self,x=17,y=100):
        upperPrimeList = list()
        # print(4)
        for i in range(x, y):
            if self.is_prime(i):
                upperPrimeList.append(i)
        # print(5)
        return upperPrimeList

    def generate_keypair(self):
        
        upperPrimeList = self.ListPrimeLessThanPhi()
        
        self.p = upperPrimeList[random.randrange(1, len(upperPrimeList))]
        
        self.q = upperPrimeList[random.randrange(1, len(upperPrimeList))]
        
        while(self.q == self.p):
            self.p = upperPrimeList[random.randrange(1, len(upperPrimeList))]
            self.q = upperPrimeList[random.randrange(1, len(upperPrimeList))]
        # print(self.p)
        # print("{}:{}".format(self.p, self.q))
        self.n = self.p * self.q
        # print(self.q)
        # Phi is the totient of n
        self.phi = (self.p - 1) * (self.q - 1)

        #random e between max(p,q) and phi and (e,p)=0 or prime
        sup = (lambda x,y:(x>y and x or y))(self.p,self.q)
        # print(self.phi)
        # print(sup)
        PrimesList = self.ListPrimeLessThanPhi(sup,self.phi)
        # print(0)
        liste = self.computeE(PrimesList)
        self.e = liste[random.randrange(0,len(liste))]

        # # Use Euclid's Algorithm to verify that e and phi(n) are comprime
        # g = self.gcd(self.e, self.phi)
        # while g != 1:
        #     self.e = random.randrange(1, self.phi)()
        #     g = self.gcd(self.e, self.phi)

        # Use Extended Euclid's Algorithm to generate the private key
        self.d = self.generateur_d(self.e, self.phi)

        # Return public and private keypair
        # Public key is (e, n) and private key is (d, n)
        self.public, self.private = ((self.e, self.n), (self.d, self.n))
        return self.private

    '''
    encrypt text message
    '''

    def encrypt(self, plaintext):
        # Unpack the key into it's components
        key, n = self.public
        # print(plaintext)
        # print(self.public)
        # Convert each letter in the plaintext to numbers based on the character using a^b mod m
        if str.isdigit(plaintext):
            return (int(plaintext)**key)%n
        cipher = [chr((ord(char) ** key) % n) for char in plaintext]
        # Return the array of bytes
        self.cypher = cipher
        # print("cypher {}".format(''.join(map(lambda x: str(x), cipher))))
        return ''.join(map(lambda x: str(x), cipher))

    # def encrypt1(self, plaintext):
    #     # Unpack the key into it's components
    #     key, n = self.private
    #     # Convert each letter in the plaintext to numbers based on the character using a^b mod m
    #     cipher = [str((char ** key) % n).encode("utf-8") for char in plaintext]
    #     # Return the array of bytes
    #     self.cypher = cipher
    #     col = cipher[0]
    #     for ele in cipher[1:]:
    #         col += ele
    #     # col = str(col)
    #     # col = col[2:-1]
    #     print("cypher {}".format(col))
    #     return col

    '''
    decrypt text message
    '''

    def decrypt(self, ciphertext):
        # Unpack the key into its components
        key, n = self.private
        # print(ciphertext)
        # print(self.private)
        # Generate the plaintext based on the ciphertext and key using a^b mod m
        if str.isdigit(ciphertext):
            return (int(ciphertext)**key)%n
        plain = [chr((ord(char) ** key) % n) for char in ciphertext]
        # Return the array of bytes as a string
        self.plaintext = ''.join(plain)
        # print(self.plaintext)
        return ''.join(plain)

    # def decrypt1(self, ciphertext):
    #     # Unpack the key into its components
    #     key, n = self.public
    #     # Generate the plaintext based on the ciphertext and key using a^b mod m
    #     plain = [str((char ** key) % n).encode("latin-1") for char in ciphertext]
    #     # Return the array of bytes
    #     col = plain[0]
    #     for ele in plain[1:]:
    #         col += ele
    #     # col = str(col)
    #     # col = col[2:-1]
    #     self.plaintext = col
    #     print("plain {}".format(col))
    #     return col


    def encryptfil(self, path, ProgId):
        self.encryptfil1(path,ProgId)

    def encryptfil1(self, path, ProgId):
        print(str(path))
        if os.path.exists(path + ".vic"):
            os.remove(path + ".vic")
        values = 0
        with open(path, 'r', encoding="latin-1") as fil:
            values = fil.readlines()
            values = len(values)
        print(values)
        # values = 0
        # with open(path, 'r', encoding="latin-1") as fil:
        #     values = fil.readlines()
        #     values = len(values)
        # print(values)
        self.editProg(ProgId, 0)
        with open(path, 'r', encoding="latin-1") as fil, open(path + ".vic", 'w+', encoding="utf-8") as outfil:
            if not fil:
                print("file not found")
            else:
                i = 0
                for line in fil:
                    i += 1
                    print(str(line))
                    outfil.write(self.encrypt(line))
                    # print(((i * 100) // (100 * values)))
                    self.editProg(
                        ProgId,
                        (
                            (int)(str(i / values)[2:4])
                            )
                        )
                self.editProg(ProgId, 100)
        # self.finit()
        print("end")

    def encryptfilA(self, path, ProgId):
        self.encryptfilA1(path,ProgId)


    def encryptfilA1(self, path, ProgId):
        print(str(path))
        if os.path.exists(path + ".vic"):
            os.remove(path + ".vic")
        values = 0
        with open(path, 'r', encoding="latin-1") as fil:
            values = fil.readlines()
            values = len(values)
        print(values)
        with open(path, 'r', encoding="latin-1") as fil, open(path + ".vic", 'w+', encoding="utf-8") as outfil:
            if not fil:
                print("file not found")
            else:
                i = 0
                for line in fil:
                    i += 1
                    print(str(line))
                    outfil.write(self.encrypt(line))
                    # print(((i * 100) // (100 * values)))
                    self.editProg(
                        ProgId,
                        (
                            (int)(str(i / values)[2:4])
                            )
                        )
                self.editProg(ProgId, 100)
        # self.finit()
        print("end")

    def decryptfil(self, path, ProgId):
        self.decryptfil1(path,ProgId)
 
    def decryptfil1(self, path, ProgId):
        print(str(path))
        pt = str(path).replace(".vic", "")
        if os.path.exists(pt):
            os.remove(pt)
        values = 0
        with open(path, 'r', encoding="latin-1") as fil:
            values = fil.readlines()
            values = len(values)
        print(values)
        with open(path, 'r', encoding="utf-8") as fil, open(pt, 'w+', encoding="latin-1") as outfil:
            if not fil:
                print("file not found")
            else:
                i = 0
                for line in fil:
                    i += 1
                    print(str(line))
                    outfil.write(self.decrypt(line))
                    # print(((i * 100) // (100 * values)))
                    self.editProg(
                        ProgId,
                        (
                            (int)(str(i / values)[2:4])
                            )
                        )
        self.editProg(ProgId, 100)
        print("end")
        # self.finit()

    def decryptfilA(self, path, ProgId):
        self.decryptfilA1(path,ProgId)

    def decryptfilA1(self, path, ProgId):
        print(str(path))
        pt = str(path).replace(".vic", "")
        if os.path.exists(pt):
            os.remove(pt)
        values = 0
        with open(path, 'r', encoding="latin-1") as fil:
            values = fil.readlines()
            values = len(values)
        print(values)
        with open(path, 'r') as fil, open(pt, 'w+', encoding="utf-8") as outfil:
            if not fil:
                print("file not found")
            else:
                i = 0
                for line in fil:
                    i += 1
                    print(str(line))
                    outfil.write(self.encrypt(line))
                    # print(((i * 100) // (100 * values)))
                    self.editProg(
                        ProgId,
                        (
                            (int)(str(i / values)[2:4])
                            )
                        )
                self.editProg(ProgId, 100)
        # self.finit()
        print("end")

    # def finit(self):
    #     t = th.Thread(target=self.finit1)
    #     t.start()

    # def finit1(self):
    #     alert = QMessageBox()
    #     alert.setIcon(QMessageBox.Information)
    #     alert.setWindowTitle("Process Degree")
    #     alert.setText('operation Complete with success!')
    #     alert.exec_()
    def is_Doc(self, string):
        if bool(re.search(".iso", string)) | bool(re.search(".daa", string)) | bool(re.search(".rar", string)) | bool(re.search(".zip", string)) | bool(re.search(".7z", string)) | bool(re.search(".iso.vic", string)) | bool(re.search(".daa.vic", string)) | bool(re.search(".rar.vic", string)) | bool(re.search(".zip.vic", string)) | bool(re.search(".7z.vic", string)):
            print("false")
            return False
        else:
            print("true")
            return True

    def is_Arch(self, string):
        if bool(re.search(".iso", string)) | bool(re.search(".daa", string)) | bool(re.search(".rar", string)) | bool(re.search(".zip", string)) | bool(re.search(".7z", string)) | bool(re.search(".iso.vic", string)) | bool(re.search(".daa.vic", string)) | bool(re.search(".rar.vic", string)) | bool(re.search(".zip.vic", string)) | bool(re.search(".7z.vic", string)):
            return True
        else:
            return False

    def encryptDoc(self, path, ProgId):
        if self.is_Doc(path):
            print("true")
            self.encryptfil(path, ProgId)

    def decryptDoc(self, path, ProgId):
        if self.is_Doc(path):
            print("true")
            self.decryptfil(path, ProgId)

    def encryptArch(self, path, ProgId):
        if self.is_Arch(path):
            print("true")
            self.encryptfilA(path, ProgId)

    def decryptArch(self, path, ProgId):
        if self.is_Arch(path):
            print("true")
            self.decryptfilA(path, ProgId)
    
    def editProg(self, ProgId, value):
        ProgId.setProperty("value", value)
# if __name__ == '__main__':
#     '''
#     Detect if the script is being run directly by the user
#     '''
#     print("RSA Encrypter/ Decrypter")
#     rsa = RSA()
#     print("Generating your public/private keypairs now . . .")
#     public, private = rsa.generate_keypair()
#     print("Your public key is " + str(public) + " and your private key is " + str(private))
#     message = input("Enter a message to encrypt with your private key: ")
#     encrypted_msg = rsa.encrypt(message)
#     print("Your encrypted message is: ")
#     print(''.join(map(lambda x: str(x), encrypted_msg)))
#     print("Decrypting message with public key " + str(public) + " . . .")
#     print("Your message is:")
#     print(rsa.decrypt(encrypted_msg))
#     print("chiffrement de document")
#     rsa.encryptDoc("test.txt")
#     input()
#     os.remove("test.txt")
#     rsa.decryptDoc("test.txt.vic")
