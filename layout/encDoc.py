# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'encDoc.ui'
#
# Created by: PyQt5 UI code generator 5.9.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets
import os
from layout import tests
from layout.Package import rsaclass
 

class encDoc(object):

    def setupUi(self, Dialog):
        self.path = ""
        self.pathout = ""
        self.pathin = ""
        Dialog.setObjectName("Dialog")
        Dialog.resize(366, 376)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("images/icons8_bluelock.ico"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        Dialog.setWindowIcon(icon)
        Dialog.setStyleSheet("background-color:#272E3F;")
        self.gridLayout = QtWidgets.QGridLayout(Dialog)
        self.gridLayout.setObjectName("gridLayout")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout_4 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        self.label = QtWidgets.QLabel(Dialog)
        self.label.setStyleSheet("color:white;")
        self.label.setObjectName("label")
        self.horizontalLayout_4.addWidget(self.label)
        self.pathLine = QtWidgets.QLineEdit(Dialog)
        self.pathLine.setEnabled(True)
        self.pathLine.setStyleSheet("color:black;\n"
"background-color:white;")
        self.pathLine.setReadOnly(True)
        self.pathLine.setObjectName("pathLine")
        self.horizontalLayout_4.addWidget(self.pathLine)
        self.browser = QtWidgets.QPushButton(Dialog)
        self.browser.clicked.connect(self.browserF)
        self.browser.setStyleSheet("background:darkgray;\n"
"color:white;")
        self.browser.setObjectName("browser")
        self.horizontalLayout_4.addWidget(self.browser)
        self.verticalLayout.addLayout(self.horizontalLayout_4)
        spacerItem = QtWidgets.QSpacerItem(343, 13, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Preferred)
        self.verticalLayout.addItem(spacerItem)
        self.encryptD = QtWidgets.QPushButton(Dialog)
        self.encryptD.clicked.connect(self.encrypt)
        self.encryptD.setStyleSheet("background:darkgray;\n"
"color:white;")
        self.encryptD.setObjectName("encryptD")
        self.verticalLayout.addWidget(self.encryptD)
        spacerItem1 = QtWidgets.QSpacerItem(20, 10, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Preferred)
        self.verticalLayout.addItem(spacerItem1)
        self.progressBar = QtWidgets.QProgressBar(Dialog)
        self.progressBar.setProperty("value", 1)
        self.progressBar.setObjectName("progressBar")
        self.verticalLayout.addWidget(self.progressBar)
        spacerItem2 = QtWidgets.QSpacerItem(343, 35, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Preferred)
        self.verticalLayout.addItem(spacerItem2)
        self.horizontalLayout_5 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_5.setObjectName("horizontalLayout_5")
        self.labelk = QtWidgets.QLabel(Dialog)
        self.labelk.setStyleSheet("color:red;")
        self.labelk.setObjectName("labelk")
        self.horizontalLayout_5.addWidget(self.labelk)
        self.publick = QtWidgets.QLineEdit(Dialog)
        self.publick.setStyleSheet("color:black;\n"
"background-color:white;")
        self.publick.setObjectName("publick")
        self.horizontalLayout_5.addWidget(self.publick)
        self.verticalLayout.addLayout(self.horizontalLayout_5)
        spacerItem3 = QtWidgets.QSpacerItem(343, 13, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Preferred)
        self.verticalLayout.addItem(spacerItem3)
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.label_2 = QtWidgets.QLabel(Dialog)
        self.label_2.setStyleSheet("color:white;")
        self.label_2.setAlignment(QtCore.Qt.AlignCenter)
        self.label_2.setObjectName("label_2")
        self.horizontalLayout_3.addWidget(self.label_2)
        self.out = QtWidgets.QLineEdit(Dialog)
        self.out.setEnabled(True)
        self.out.setStyleSheet("color:black;\n"
"background-color:white;")
        self.out.setReadOnly(True)
        self.out.setObjectName("out")
        self.horizontalLayout_3.addWidget(self.out)
        self.verticalLayout.addLayout(self.horizontalLayout_3)
        spacerItem4 = QtWidgets.QSpacerItem(343, 13, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Preferred)
        self.verticalLayout.addItem(spacerItem4)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.save = QtWidgets.QPushButton(Dialog)
        self.save.setStyleSheet("background:darkgray;\n"
"color:white;")
        self.save.setObjectName("save")
        self.horizontalLayout_2.addWidget(self.save)
        self.saveAs = QtWidgets.QPushButton(Dialog)
        self.saveAs.clicked.connect(self.browserD)
        self.saveAs.setStyleSheet("background:darkgray;\n"
"color:white;")
        self.saveAs.setObjectName("saveAs")
        self.horizontalLayout_2.addWidget(self.saveAs)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        spacerItem5 = QtWidgets.QSpacerItem(343, 13, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Preferred)
        self.verticalLayout.addItem(spacerItem5)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.email = QtWidgets.QLineEdit(Dialog)
        self.email.setStyleSheet("color:black;\n"
"background-color:white;")
        self.email.setObjectName("email")
        self.horizontalLayout.addWidget(self.email)
        self.send = QtWidgets.QPushButton(Dialog)
        self.send.setStyleSheet("background:darkgray;\n"
"color:white;")
        self.send.setObjectName("send")
        self.horizontalLayout.addWidget(self.send)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.gridLayout.addLayout(self.verticalLayout, 0, 0, 1, 1)

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def browserF(self):
        fname = tests.Example().showDialog()
        self.pathin = fname
        self.path = (str(fname).split("/"))[-1]
        print(self.path)
        self.pathLine.setText(fname)
        self.out.setText(fname + ".vic")

    def browserD(self):
        dname = tests.Example().showDialogD()
        if dname != "":
                self.out.setText(dname + self.path + ".vic")
        self.pathout = dname + self.path + ".vic"
        self.saveAsf()

    def encrypt(self):
        rsa = rsaclass.RSA()
        self.publick.setText(str(rsa.generate_keypair()))
        rsa.encryptDoc(self.pathin, self.progressBar)
        print(0)

    def saveAsf(self):
        if os.path.exists(self.pathout):
            os.remove(self.pathout)
        os.rename(self.pathin + ".vic", self.pathout)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Document Encrypt"))
        self.label.setText(_translate("Dialog", "Path ::"))
        self.pathLine.setPlaceholderText(_translate("Dialog", "Here is your file path location"))
        self.browser.setText(_translate("Dialog", "Browser"))
        self.encryptD.setText(_translate("Dialog", "Encrypt"))
        self.labelk.setText(_translate("Dialog", "Public key to share ::"))
        self.label_2.setText(_translate("Dialog", "Out ::"))
        self.out.setPlaceholderText(_translate("Dialog", "File is your out file path location"))
        self.save.setText(_translate("Dialog", "save"))
        self.saveAs.setText(_translate("Dialog", "save as"))
        self.email.setPlaceholderText(_translate("Dialog", "enter the email of recever"))
        self.send.setText(_translate("Dialog", "send"))





'''
    here is
'''





# if __name__ == "__main__":
#     import sys
#     app = QtWidgets.QApplication(sys.argv)
#     Dialog = QtWidgets.QDialog()
#     ui = encDoc()
#     ui.setupUi(Dialog)
#     Dialog.show()
#     sys.exit(app.exec_()) (3593, 8633)
