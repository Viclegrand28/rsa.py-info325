# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'LicenceSite.ui'
#
# Created by: PyQt5 UI code generator 5.9.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets, QtWebKitWidgets


class Ui_LicenecSite(object):

    def setupUi(self, LicenecSite):
        LicenecSite.setObjectName("LicenecSite")
        LicenecSite.resize(405, 300)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("images/icons8_bluelock.ico"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        LicenecSite.setWindowIcon(icon)
        self.gridLayout = QtWidgets.QGridLayout(LicenecSite)
        self.gridLayout.setObjectName("gridLayout")
        self.BuyLicence = QtWebKitWidgets.QWebView(LicenecSite)
        self.BuyLicence.setAutoFillBackground(True)
        self.BuyLicence.setStyleSheet("background:white;")
        self.BuyLicence.setUrl(QtCore.QUrl("about:blank"))
        self.BuyLicence.setObjectName("BuyLicence")
        self.gridLayout.addWidget(self.BuyLicence, 0, 0, 1, 1)

        self.retranslateUi(LicenecSite)
        QtCore.QMetaObject.connectSlotsByName(LicenecSite)

    def retranslateUi(self, LicenecSite):
        _translate = QtCore.QCoreApplication.translate
        LicenecSite.setWindowTitle(_translate("LicenecSite", "Licence"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    LicenecSite = QtWidgets.QDialog()
    ui = Ui_LicenecSite()
    ui.setupUi(LicenecSite)
    LicenecSite.show()
    sys.exit(app.exec_())
