# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'decDoc.ui'
#
# Created by: PyQt5 UI code generator 5.9.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Dialog(object):

    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(435, 377)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("../images/icons8_bluelock.ico"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        Dialog.setWindowIcon(icon)
        Dialog.setStyleSheet("background-color:#272E3F;")
        self.gridLayout = QtWidgets.QGridLayout(Dialog)
        self.gridLayout.setObjectName("gridLayout")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout_4 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        self.label = QtWidgets.QLabel(Dialog)
        self.label.setStyleSheet("color:white;")
        self.label.setObjectName("label")
        self.horizontalLayout_4.addWidget(self.label)
        self.pathLine = QtWidgets.QLineEdit(Dialog)
        self.pathLine.setEnabled(True)
        self.pathLine.setStyleSheet("color:black;\n"
"background-color:white;")
        self.pathLine.setReadOnly(True)
        self.pathLine.setObjectName("pathLine")
        self.horizontalLayout_4.addWidget(self.pathLine)
        self.browser = QtWidgets.QPushButton(Dialog)
        self.browser.setStyleSheet("background:darkgray;\n"
"color:white;")
        self.browser.setObjectName("browser")
        self.horizontalLayout_4.addWidget(self.browser)
        self.verticalLayout.addLayout(self.horizontalLayout_4)
        spacerItem = QtWidgets.QSpacerItem(20, 10, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Preferred)
        self.verticalLayout.addItem(spacerItem)
        self.horizontalLayout_7 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_7.setObjectName("horizontalLayout_7")
        self.label_5 = QtWidgets.QLabel(Dialog)
        self.label_5.setStyleSheet("color:red;")
        self.label_5.setObjectName("label_5")
        self.horizontalLayout_7.addWidget(self.label_5)
        self.publicK = QtWidgets.QLineEdit(Dialog)
        self.publicK.setStyleSheet("color:black;\n"
"background-color:white;")
        self.publicK.setObjectName("publicK")
        self.horizontalLayout_7.addWidget(self.publicK)
        self.verticalLayout.addLayout(self.horizontalLayout_7)
        spacerItem1 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Preferred)
        self.verticalLayout.addItem(spacerItem1)
        self.decryptD = QtWidgets.QPushButton(Dialog)
        self.decryptD.setStyleSheet("background:darkgray;\n"
"color:white;")
        self.decryptD.setObjectName("decryptD")
        self.verticalLayout.addWidget(self.decryptD)
        spacerItem2 = QtWidgets.QSpacerItem(20, 10, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Preferred)
        self.verticalLayout.addItem(spacerItem2)
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.label_2 = QtWidgets.QLabel(Dialog)
        self.label_2.setStyleSheet("color:white;")
        self.label_2.setAlignment(QtCore.Qt.AlignCenter)
        self.label_2.setObjectName("label_2")
        self.horizontalLayout_3.addWidget(self.label_2)
        self.out = QtWidgets.QLineEdit(Dialog)
        self.out.setEnabled(True)
        self.out.setReadOnly(True)
        self.out.setObjectName("out")
        self.horizontalLayout_3.addWidget(self.out)
        self.verticalLayout.addLayout(self.horizontalLayout_3)
        spacerItem3 = QtWidgets.QSpacerItem(20, 10, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Preferred)
        self.verticalLayout.addItem(spacerItem3)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.save = QtWidgets.QPushButton(Dialog)
        self.save.setStyleSheet("background:darkgray;\n"
"color:white;")
        self.save.setObjectName("save")
        self.horizontalLayout_2.addWidget(self.save)
        self.saveAs = QtWidgets.QPushButton(Dialog)
        self.saveAs.setStyleSheet("background:darkgray;\n"
"color:white;")
        self.saveAs.setObjectName("saveAs")
        self.horizontalLayout_2.addWidget(self.saveAs)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        spacerItem4 = QtWidgets.QSpacerItem(20, 10, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Preferred)
        self.verticalLayout.addItem(spacerItem4)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.email = QtWidgets.QLineEdit(Dialog)
        self.email.setStyleSheet("color:black;\n"
"background-color:white;")
        self.email.setObjectName("email")
        self.horizontalLayout.addWidget(self.email)
        self.send = QtWidgets.QPushButton(Dialog)
        self.send.setStyleSheet("background:darkgray;\n"
"color:white;")
        self.send.setObjectName("send")
        self.horizontalLayout.addWidget(self.send)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.gridLayout.addLayout(self.verticalLayout, 0, 0, 1, 1)

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Dialog"))
        self.label.setText(_translate("Dialog", "Path ::"))
        self.pathLine.setPlaceholderText(_translate("Dialog", "Here is your file path location"))
        self.browser.setText(_translate("Dialog", "Browser"))
        self.label_5.setText(_translate("Dialog", "Public key to share ::"))
        self.decryptD.setText(_translate("Dialog", "Decrypt"))
        self.label_2.setText(_translate("Dialog", "Out ::"))
        self.out.setStyleSheet(_translate("Dialog", "color:black;\n"
"background-color:white;"))
        self.out.setPlaceholderText(_translate("Dialog", "File is your out file path location"))
        self.save.setText(_translate("Dialog", "save"))
        self.saveAs.setText(_translate("Dialog", "save as"))
        self.email.setPlaceholderText(_translate("Dialog", "enter the email of recever"))
        self.send.setText(_translate("Dialog", "send"))

# if __name__ == "__main__":
#     import sys
#     app = QtWidgets.QApplication(sys.argv)
#     Dialog = QtWidgets.QDialog()
#     ui = Ui_Dialog()
#     ui.setupUi(Dialog)
#     Dialog.show()
#     sys.exit(app.exec_())

