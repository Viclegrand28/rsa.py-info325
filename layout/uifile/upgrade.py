# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '.\upgrade.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Upgrade(object):
    def setupUi(self, Upgrade):
        Upgrade.setObjectName("Upgrade")
        Upgrade.resize(357, 221)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(".\\../../images/icons8_bluelock.ico"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        Upgrade.setWindowIcon(icon)
        Upgrade.setStyleSheet("background-color:#272E3F;")
        self.gridLayout = QtWidgets.QGridLayout(Upgrade)
        self.gridLayout.setObjectName("gridLayout")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.label_3 = QtWidgets.QLabel(Upgrade)
        self.label_3.setText("")
        self.label_3.setPixmap(QtGui.QPixmap(".\\../../images/icons8_heart_balloon.ico"))
        self.label_3.setAlignment(QtCore.Qt.AlignCenter)
        self.label_3.setObjectName("label_3")
        self.horizontalLayout.addWidget(self.label_3)
        self.label_2 = QtWidgets.QLabel(Upgrade)
        self.label_2.setText("")
        self.label_2.setPixmap(QtGui.QPixmap(".\\../../images/icons8_close_up_mode.ico"))
        self.label_2.setAlignment(QtCore.Qt.AlignCenter)
        self.label_2.setObjectName("label_2")
        self.horizontalLayout.addWidget(self.label_2)
        self.label_4 = QtWidgets.QLabel(Upgrade)
        self.label_4.setText("")
        self.label_4.setPixmap(QtGui.QPixmap(".\\../../images/icons8_flower.ico"))
        self.label_4.setAlignment(QtCore.Qt.AlignCenter)
        self.label_4.setObjectName("label_4")
        self.horizontalLayout.addWidget(self.label_4)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.label = QtWidgets.QLabel(Upgrade)
        self.label.setText("")
        self.label.setPixmap(QtGui.QPixmap(".\\../../images/icons8_bluelock.ico"))
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.label.setObjectName("label")
        self.verticalLayout.addWidget(self.label)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.label_5 = QtWidgets.QLabel(Upgrade)
        self.label_5.setText("")
        self.label_5.setPixmap(QtGui.QPixmap(".\\../../images/icons8_applause.ico"))
        self.label_5.setAlignment(QtCore.Qt.AlignCenter)
        self.label_5.setObjectName("label_5")
        self.horizontalLayout_2.addWidget(self.label_5)
        self.label_6 = QtWidgets.QLabel(Upgrade)
        self.label_6.setObjectName("label_6")
        self.horizontalLayout_2.addWidget(self.label_6)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        self.gridLayout.addLayout(self.verticalLayout, 0, 0, 1, 1)

        self.retranslateUi(Upgrade)
        QtCore.QMetaObject.connectSlotsByName(Upgrade)

    def retranslateUi(self, Upgrade):
        _translate = QtCore.QCoreApplication.translate
        Upgrade.setWindowTitle(_translate("Upgrade", "Upgrade"))
        self.label_6.setText(_translate("Upgrade", "<html><head/><body><p align=\"center\"><span style=\" font-size:10pt; font-weight:600; color:#ffffff;\">Congratulate, You have the latest</span></p><p align=\"center\"><span style=\" font-size:10pt; font-weight:600; color:#ffffff;\">version of the software</span></p></body></html>"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Upgrade = QtWidgets.QDialog()
    ui = Ui_Upgrade()
    ui.setupUi(Upgrade)
    Upgrade.show()
    sys.exit(app.exec_())
