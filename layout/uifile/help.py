# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '.\help.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Help(object):
    def setupUi(self, Help):
        Help.setObjectName("Help")
        Help.resize(604, 597)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(".\\../../images/icons8_bluelock.ico"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        Help.setWindowIcon(icon)
        Help.setStyleSheet("background-color:#272E3F;")
        self.gridLayout = QtWidgets.QGridLayout(Help)
        self.gridLayout.setObjectName("gridLayout")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.label = QtWidgets.QLabel(Help)
        self.label.setText("")
        self.label.setPixmap(QtGui.QPixmap(".\\../../images/logo3.png"))
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.label.setObjectName("label")
        self.verticalLayout.addWidget(self.label)
        self.textBrowser = QtWidgets.QTextBrowser(Help)
        self.textBrowser.setObjectName("textBrowser")
        self.verticalLayout.addWidget(self.textBrowser)
        self.gridLayout.addLayout(self.verticalLayout, 0, 0, 1, 1)

        self.retranslateUi(Help)
        QtCore.QMetaObject.connectSlotsByName(Help)

    def retranslateUi(self, Help):
        _translate = QtCore.QCoreApplication.translate
        Help.setWindowTitle(_translate("Help", "Help"))
        self.textBrowser.setHtml(_translate("Help", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'MS Shell Dlg 2\'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p align=\"center\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:10pt; font-weight:600; font-style:italic;\">This software have to be the helpfull method to garant the secure transmission of data between Internet using RSA Algorithm.</span></p>\n"
"<p align=\"center\" style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:10pt; font-weight:600;\">The first module can help you to secure your plaintext message to send</span></p>\n"
"<p align=\"center\" style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:10pt; font-style:italic;\">to use this module, first step select your use method, Encrypt | Decrypt</span></p>\n"
"<p align=\"center\" style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:10pt; font-style:italic;\">* </span><span style=\" font-size:10pt; font-weight:600; font-style:italic;\">Encrypt</span><span style=\" font-size:10pt; font-style:italic;\"> : Write your plaintext message and right press, encrypt button. the module will return the public key to decrypt the cypherText message. This key may be give to your internaut with so much security and nextly you can transmit your message between your network chat system.</span></p>\n"
"<p align=\"center\" style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:10pt; font-style:italic;\">* </span><span style=\" font-size:10pt; font-weight:600; font-style:italic;\">Decrypt</span><span style=\" font-size:10pt; font-style:italic;\"> : Hit the public key in the edit space call keypairs and next, copy and paste your cypherText .</span></p>\n"
"<p align=\"center\" style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:10pt; font-weight:600;\">The next functions can be help you to secure and files and directory </span></p>\n"
"<p align=\"center\" style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:10pt; font-style:italic;\">It\'s working with the same idea, </span></p>\n"
"<p align=\"center\" style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:10pt; font-style:italic;\">* You browser the</span><span style=\" font-size:10pt; font-weight:600; font-style:italic;\"> path file directory</span><span style=\" font-size:10pt; font-style:italic;\"> or</span><span style=\" font-size:10pt; font-weight:600; font-style:italic;\"> path directory</span><span style=\" font-size:10pt; font-style:italic;\">, if it\'s encrypt usage, we just press to the enccrypt button else hit before the </span><span style=\" font-size:10pt; font-weight:600; font-style:italic;\">public key</span><span style=\" font-size:10pt; font-style:italic;\"> and press decrypt button.</span></p>\n"
"<p align=\"center\" style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:10pt; font-style:italic;\">* The functions of encrypt of decrypt file create automaticly the cypher file in the same directory but you can change the out path directory.</span></p>\n"
"<p align=\"center\" style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:10pt; font-style:italic;\">* Nowly, the directory module is in maintains but we will fewerly push the update of application.</span></p>\n"
"<p align=\"center\" style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:10pt; font-style:italic;\">So thank you for your fidelity and injoy your original software made by </span><span style=\" font-size:10pt; font-weight:600; font-style:italic;\">DTVN </span><span style=\" font-size:10pt; font-style:italic;\">in the ICT-RSA SECURITY context and all the staff.</span></p>\n"
"<p align=\"center\" style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:10pt; font-weight:600; font-style:italic;\">Crew members</span><span style=\" font-size:10pt; font-style:italic;\"> :</span></p>\n"
"<p align=\"center\" style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:10pt; font-weight:600; font-style:italic;\">--&gt;     DJIEMBOU TIENTCHEU Victor Nico 17T2051</span></p>\n"
"<p align=\"center\" style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'TimesNewRomanPSMT\'; font-size:10pt; font-weight:600; font-style:italic; color:#000000;\">--&gt;    BKWEDOU-NGAMENI-Bechir Delibes 17T2032</span><span style=\" font-family:\'TimesNewRomanPSMT\'; font-size:10pt; color:#000000;\"> </span></p>\n"
"<p align=\"center\" style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:10pt; font-weight:600; font-style:italic;\">--&gt;     NGUEYAP YOUBISSI Lamblin Fabrice </span><span style=\" font-family:\'TimesNewRomanPSMT\'; font-size:10pt; font-weight:600; font-style:italic; color:#000000;\">17A2289</span><span style=\" font-family:\'TimesNewRomanPSMT\'; font-size:10pt; color:#000000;\"><br /><br /></span></p></body></html>"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Help = QtWidgets.QDialog()
    ui = Ui_Help()
    ui.setupUi(Help)
    Help.show()
    sys.exit(app.exec_())
