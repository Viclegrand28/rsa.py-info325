import urllib


def is_internet():
    """
    Query internet using python 
    :return
    """
    try:
        urllib.request.urlopen('http://www.google.com', timeout=1)
        return True
    except:
        return False


if is_internet():
    print("Internet is active")
else:
    print("Internet is disconnect")
