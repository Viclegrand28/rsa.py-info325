# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'encText.ui'
#
# Created by: PyQt5 UI code generator 5.9.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets
from layout.Package import rsaclass
 
class encText(object):
  
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(364, 463)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("images/icons8_bluelock.ico"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        Dialog.setWindowIcon(icon)
        Dialog.setStyleSheet("background-color:#272E3F;")
        self.gridLayout = QtWidgets.QGridLayout(Dialog)
        self.gridLayout.setObjectName("gridLayout")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.plainTextEdit = QtWidgets.QPlainTextEdit(Dialog)
        self.plainTextEdit.setPlaceholderText("Please Hit your message to crypt by RSA algorithm")
        self.plainTextEdit.setStyleSheet("background-color:white;\n"
"color:black;\n"
"border-style:outset;\n"
"border-width:2px;\n"
"font: 22pt \"MS Shell Dlg 2\";\n"
"border-radius:10px;\n"
"font: bold 14px;\n"
"min-width: 10em;\n"
"padding:6px;")
        self.plainTextEdit.setObjectName("plainTextEdit")
        self.verticalLayout.addWidget(self.plainTextEdit)
        self.btn_encryptT = QtWidgets.QPushButton(Dialog)
        self.btn_encryptT.clicked.connect(self.crypt)
        self.btn_encryptT.setStyleSheet("background:darkgray;")
        self.btn_encryptT.setObjectName("btn_encryptT")
        self.verticalLayout.addWidget(self.btn_encryptT)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.label = QtWidgets.QLabel(Dialog)
        self.label.setStyleSheet("color:red;")
        self.label.setObjectName("label")
        self.horizontalLayout.addWidget(self.label)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.lineEdit = QtWidgets.QLineEdit(Dialog)
        self.lineEdit.setPlaceholderText("Transmitter key")
        self.lineEdit.setEnabled(False)
        self.lineEdit.setStyleSheet("background-color:white;\n"
"color:black;\n"
"border-style:outset;\n"
"border-width:2px;\n"
"border-radius:10px;\n"
"font: bold 10px;\n"
"min-width: 10em;\n"
"padding:6px;")
        self.lineEdit.setObjectName("lineEdit")
        self.horizontalLayout.addWidget(self.lineEdit)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.plainTextEdit_2 = QtWidgets.QPlainTextEdit(Dialog)
        self.plainTextEdit_2.setPlaceholderText("This is result encrypt message")
        self.plainTextEdit_2.setEnabled(False)
        self.plainTextEdit_2.setStyleSheet("background-color:white;\n"
"color:black;\n"
"border-style:outset;\n"
"border-width:2px;\n"
"border-radius:10px;\n"
"font: bold 14px;\n"
"min-width: 10em;\n"
"padding:6px;")
        self.plainTextEdit_2.setObjectName("plainTextEdit_2")
        self.verticalLayout.addWidget(self.plainTextEdit_2)
        self.gridLayout.addLayout(self.verticalLayout, 0, 0, 1, 1)

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def crypt(self):
        rsa = rsaclass.RSA()
        plaintext = self.plainTextEdit.toPlainText()
        self.lineEdit.setText(str(rsa.generate_keypair()))
        self.lineEdit.setEnabled(True)
        self.plainTextEdit_2.setPlainText(str(rsa.encrypt(plaintext)))
        self.plainTextEdit_2.setEnabled(True)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Text-Encrypt"))
        self.btn_encryptT.setText(_translate("Dialog", "Encrypt"))
        self.label.setText(_translate("Dialog", "<html><head/><body><p><span style=\" font-size:12pt; font-weight:600;\">Keypairs     ::</span></p></body></html>"))

# if __name__ == "__main__":
#     import sys
#     app = QtWidgets.QApplication(sys.argv)
#     Dialog = QtWidgets.QDialog()
#     ui = encText()
#     ui.setupUi(Dialog)
#     Dialog.show()
#     sys.exit(app.exec_())

#  ((104347, 292121), (97219, 292121))
