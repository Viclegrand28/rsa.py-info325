# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '.\register.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QMessageBox
import subprocess
import webbrowser
import sys
import pymysql as mdb
from layout.Package import licence
import threading as th
from PyQt5.QtWidgets import *
from uuid import getnode as get_mac
##################################################################
######### Variable Global essentiel pour la gestion de licence ###
##################################################################
mac = ':'.join(("%012X" % get_mac())[i:i+2] for i in range(0, 12, 2))


class Ui_Register(object):

    def setupUi(self, Register):
        Register.setObjectName("Register")
        Register.resize(408, 332)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(Register.sizePolicy().hasHeightForWidth())
        Register.setSizePolicy(sizePolicy)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("images/icons8_bluelock.ico"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        Register.setWindowIcon(icon)
        Register.setStyleSheet("background-color:#272E3F;")
        Register.setSizeGripEnabled(False)
        Register.setModal(False)
        self.gridLayout = QtWidgets.QGridLayout(Register)
        self.gridLayout.setObjectName("gridLayout")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.label_2 = QtWidgets.QLabel(Register)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_2.sizePolicy().hasHeightForWidth())
        self.label_2.setSizePolicy(sizePolicy)
        self.label_2.setStyleSheet("color:white;")
        self.label_2.setAlignment(QtCore.Qt.AlignCenter)
        self.label_2.setWordWrap(True)
        self.label_2.setObjectName("label_2")
        self.verticalLayout.addWidget(self.label_2)
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Maximum)
        self.verticalLayout.addItem(spacerItem)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.label = QtWidgets.QLabel(Register)
        self.label.setStyleSheet("color:white;")
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.label.setObjectName("label")
        self.horizontalLayout.addWidget(self.label)
        self.licencekey = QtWidgets.QLineEdit(Register)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.licencekey.sizePolicy().hasHeightForWidth())
        self.licencekey.setSizePolicy(sizePolicy)
        self.licencekey.setStyleSheet("color:white;")
        self.licencekey.setObjectName("licencekey")
        self.horizontalLayout.addWidget(self.licencekey)
        self.verticalLayout.addLayout(self.horizontalLayout)
        spacerItem1 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Maximum)
        self.verticalLayout.addItem(spacerItem1)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.label_3 = QtWidgets.QLabel(Register)
        self.label_3.setStyleSheet("color:white;\n"
"")
        self.label_3.setAlignment(QtCore.Qt.AlignCenter)
        self.label_3.setObjectName("label_3")
        self.horizontalLayout_2.addWidget(self.label_3)
        spacerItem2 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem2)
        self.email = QtWidgets.QLineEdit(Register)
        self.email.setStyleSheet("color:white;")
        self.email.setObjectName("email")
        self.horizontalLayout_2.addWidget(self.email)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        spacerItem3 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Maximum)
        self.result = QtWidgets.QLabel(Register)
        self.result.setStyleSheet("color:red;")
        self.result.setObjectName("result")
        self.verticalLayout.addWidget(self.result)
        spacerItem4 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem3)
        self.loginIn = QtWidgets.QPushButton(Register)
        self.loginIn.clicked.connect(self.login)
        self.loginIn.setStyleSheet("color:white;\n"
"background-color:#272E3F;")
        self.loginIn.setObjectName("loginIn")
        self.verticalLayout.addWidget(self.loginIn)
        self.result = QtWidgets.QLabel(Register)
        self.result.setStyleSheet("color:red;")
        self.result.setObjectName("result")
        self.verticalLayout.addWidget(self.result)
        spacerItem4 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem4)
        self.btn_active = QtWidgets.QPushButton(Register)
        self.btn_active.clicked.connect(self.active)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.btn_active.sizePolicy().hasHeightForWidth())
        self.btn_active.setSizePolicy(sizePolicy)
        self.btn_active.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.btn_active.setStyleSheet("color:black;\n"
"background-color:white;")
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap("images/icons8_password1.ico"), QtGui.QIcon.Normal, QtGui.QIcon.On)
        self.btn_active.setIcon(icon1)
        self.btn_active.setObjectName("btn_active")
        self.verticalLayout.addWidget(self.btn_active)
        self.gridLayout.addLayout(self.verticalLayout, 0, 0, 1, 1)

        self.retranslateUi(Register)
        QtCore.QMetaObject.connectSlotsByName(Register)

    def login(self):
        t = th.Thread(target=self.login1)
        t.start()
    def login1(self):
        url = 'http://192.168.0.101/ICTRSA/View/authView.php'
        if sys.platform == 'darwin':  # in case of OS X
            subprocess.Popen(['open', url])
        elif sys.platform == 'linux':
            chrome_path = '/usr/lib/firefox-esr/firefox-esr %s'
            webbrowser.get(chrome_path).open(url)
        else:
            webbrowser.open_new_tab(url)

    def active(self):
        t = th.Thread(target=self.active1)
        t.start()
    def active1(self):
        with  mdb.connect(host="192.168.0.101", user="Victor", passwd="Victor", db="ict_rsa") as cursor:
            # cursor = con.cursor()
            sql = "SELECT `LicenceUsed` FROM `account` WHERE `Email_Client`= '" + self.email.text() + "' AND  `Licence`= '" + self.licencekey.text() + "'"
            print(sql)
            cursor.execute(sql)
            if cursor.fetchone():
                try:
                    cursor.execute(sql)
                    result = cursor.fetchone()[0]
                    if result == "False":
                        self.result.setStyleSheet("color:green;")
                        self.result.setText("ICT-RSA SECURITY activation is a success !!!")
                        lic = licence.licence()
                        lic.setLic(mac)
                        lic.write("licence")
                        sql1 = "UPDATE `account` SET `LicenceUsed` = 'True' WHERE `Email_Client`= '" + self.email.text() + "' AND  `Licence`= '" + self.licencekey.text() + "'"
                        if cursor.execute(sql1):
                            self.licencekey.setText("")
                            self.email.setText("")
                    else:
                        self.result.setStyleSheet("color:red;")
                        self.result.setText("ICT-RSA SECURITY can't be activated, because the maximum of users has been reached\nPlease close before retrying")
                except:
                    self.result.setStyleSheet("color:red;")
                    self.result.setText("An error has occured while connecting to server, please close and make sure that \nyou a connected to a network")

                # self.close()
            else:
                self.result.setStyleSheet("color:red;")
                self.licencekey.setText("")
                self.licencekey.setPlaceholderText("Invalide licence for this email")
                self.result.setText("ICT-RSA SECURITY invalid licence account\nPlease close before retrying")

    def retranslateUi(self, Register):
        _translate = QtCore.QCoreApplication.translate
        Register.setWindowTitle(_translate("Register", "Register"))
        self.label_2.setText(_translate("Register", "<html><head/><body><p><span style=\" font-size:10pt; font-weight:600; font-style:italic;\">Please Hint your licence to purchare the softwware</span></p></body></html>"))
        self.label.setText(_translate("Register", "<html><head/><body><p><span style=\" font-size:10pt; font-weight:600;\">Licence key ::</span></p></body></html>"))
        self.licencekey.setPlaceholderText(_translate("Register", "AAAAA-BBBBB-CCCCC-DDDDD"))
        self.label_3.setText(_translate("Register", "Email ::   "))
        self.email.setPlaceholderText(_translate("Register", "Please hit your email"))
        self.loginIn.setText(_translate("Register", "Don\'t you have your account ?"))
        self.btn_active.setText(_translate("Register", "Active"))


# if __name__ == "__main__":
#     import sys
#     app = QtWidgets.QApplication(sys.argv)
#     Register = QtWidgets.QDialog()
#     ui = Ui_Register()
#     ui.setupUi(Register)
#     Register.show()
#     sys.exit(app.exec_())
