# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'decText.ui'
#
# Created by: PyQt5 UI code generator 5.9.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets
from layout.Package import rsaclass
 

class decText(object):

    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(362, 463)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("images/icons8_bluelock.ico"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        Dialog.setWindowIcon(icon)
        Dialog.setStyleSheet("background-color:#272E3F;")
        self.gridLayout = QtWidgets.QGridLayout(Dialog)
        self.gridLayout.setObjectName("gridLayout")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.label = QtWidgets.QLabel(Dialog)
        self.label.setStyleSheet("color:red;")
        self.label.setObjectName("label")
        self.horizontalLayout.addWidget(self.label)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.lineEdit = QtWidgets.QLineEdit(Dialog)
        self.lineEdit.setPlaceholderText("Hit public key")
        self.lineEdit.setEnabled(True)
        self.lineEdit.setStyleSheet("background-color:white;\n"
"color:black;\n"
"border-style:outset;\n"
"border-width:2px;\n"
"border-radius:10px;\n"
"font: bold 10px;\n"
"min-width: 10em;\n"
"padding:6px;")
        self.lineEdit.setObjectName("lineEdit")
        self.horizontalLayout.addWidget(self.lineEdit)
        self.gridLayout.addLayout(self.horizontalLayout, 0, 0, 1, 1)
        self.plainTextEdit = QtWidgets.QPlainTextEdit(Dialog)
        self.plainTextEdit.setPlaceholderText("Please Hit your message to crypt by RSA algorithm")
        self.plainTextEdit.setStyleSheet("background-color:white;\n"
"color:black;\n"
"border-style:outset;\n"
"border-width:2px;\n"
"font: 22pt \"MS Shell Dlg 2\";\n"
"border-radius:10px;\n"
"font: bold 14px;\n"
"min-width: 10em;\n"
"padding:6px;")
        self.plainTextEdit.setObjectName("plainTextEdit")
        self.gridLayout.addWidget(self.plainTextEdit, 1, 0, 1, 1)
        self.btn_decryptT = QtWidgets.QPushButton(Dialog)
        self.btn_decryptT.clicked.connect(self.decrypt)
        self.btn_decryptT.setStyleSheet("background:darkgray;")
        self.btn_decryptT.setObjectName("btn_decryptT")
        self.gridLayout.addWidget(self.btn_decryptT, 2, 0, 1, 1)
        self.plainTextEdit_2 = QtWidgets.QPlainTextEdit(Dialog)
        self.plainTextEdit_2.setPlaceholderText("This is result encrypt message")
        self.plainTextEdit_2.setEnabled(False)
        self.plainTextEdit_2.setStyleSheet("background-color:white;\n"
"color:black;\n"
"border-style:outset;\n"
"border-width:2px;\n"
"border-radius:10px;\n"
"font: bold 14px;\n"
"min-width: 10em;\n"
"padding:6px;")
        self.plainTextEdit_2.setObjectName("plainTextEdit_2")
        self.gridLayout.addWidget(self.plainTextEdit_2, 3, 0, 1, 1)
        self.label.raise_()
        self.plainTextEdit.raise_()
        self.btn_decryptT.raise_()
        self.plainTextEdit_2.raise_()

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def decrypt(self):
        rsa = rsaclass.RSA()
        plaintext = self.lineEdit.text()
        # print(plaintext)
        #  (485559, 585223)
        plaintext = plaintext.strip()
        Tab = plaintext.split(",")
        e = int(Tab[0][1:])
        n = int((Tab[1].strip())[:-1])
        pk = (e, n)
        rsa.setPrk(pk)
        self.plainTextEdit_2.setPlainText(str(rsa.decrypt(self.plainTextEdit.toPlainText())))
        self.plainTextEdit_2.setEnabled(True)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Text-Decrypt"))
        self.label.setText(_translate("Dialog", "<html><head/><body><p><span style=\" font-size:12pt; font-weight:600;\">Keypairs     ::</span></p></body></html>"))
        self.btn_decryptT.setText(_translate("Dialog", "Decrypt"))

# if __name__ == "__main__":
#     import sys
#     app = QtWidgets.QApplication(sys.argv)
#     Dialog = QtWidgets.QDialog()
#     ui = decText()
#     ui.setupUi(Dialog)
#     Dialog.show()
#     sys.exit(app.exec_())
