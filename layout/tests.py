#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
ZetCode PyQt5 tutorial

In this example, we select a file with a
QFileDialog and display its contents
in a QTextEdit.

Author: Jan Bodnar
Website: zetcode.com
Last edited: August 2017
"""

from PyQt5.QtWidgets import (QMainWindow, QTextEdit,
    QAction, QFileDialog, QApplication)
from PyQt5.QtGui import QIcon
import sys


class Example(QMainWindow):

    def __init__(self):
        super().__init__()

        # self.initUI()

    def initUI(self):

        self.textEdit = QTextEdit()
        self.setCentralWidget(self.textEdit)
        self.statusBar()

        openFile = QAction(QIcon('open.png'), 'Open', self)
        openFile.setShortcut('Ctrl+O')
        openFile.setStatusTip('Open new File')
        openFile.triggered.connect(self.showDialogD)

        menubar = self.menuBar()
        fileMenu = menubar.addMenu('&File')
        fileMenu.addAction(openFile)

        self.setGeometry(300, 300, 350, 300)
        self.setWindowTitle('File dialog')
        self.show()

    def showDialog(self):
        files = QFileDialog.getOpenFileName(self, 'Open file', '/home')
        if str(files) != "":
            return files[0]
        else:
            return ""

    def DshowDialog(self):
        files = QFileDialog.getOpenFileName(self, 'Open file', '/home',"Encrypt Files (*.vic)")
        if str(files) != "":
            return files[0]
        else:
            return ""

    def showDialogD(self):
        files = QFileDialog.getExistingDirectory(self, 'select directory')
        print(files)
        if str(files) != "":
            return files + "/"
        else:
            return ""

    def saveDialog(self):
        dire = QFileDialog.getExistingDirectory(self, 'select directory')
        print(dire)
        if str(dire) != "":
            return dire + "/"
        else:
            return ""


if __name__ == '__main__':

    app = QApplication(sys.argv)
    ex = Example()
    sys.exit(app.exec_())
